import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { LucidusModule } from '@solit/lucidus';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ExampleChildComponent } from './example-child/example-child.component';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientModule } from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent,
    ExampleChildComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LucidusModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
