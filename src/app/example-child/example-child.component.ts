import {Component, Input, OnInit} from '@angular/core';

type comboboxInputClass = '' | '_modified';
type comboboxOptionClass = '' | '_modified';

@Component({
  selector: 'app-example-child',
  templateUrl: './example-child.component.html',
  styleUrls: ['./example-child.component.scss']
})
export class ExampleChildComponent implements OnInit {

  @Input() comboboxInputClass: comboboxInputClass = '';
  @Input() comboboxOptionClass: comboboxOptionClass = '';

  constructor() { }

  ngOnInit(): void {
  }

}
