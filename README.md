# Angular styles

## Установка

### Развернуть проект:

```
npm install
```

### Запуск:

```
npm run start
```

## ***

### Общие правила

1. Использование переменных для font-family, font-size, font-weight, line-height, letter-spacing, color,
   background-color, border-color, transition и др.
2. margin-top и margin-left не используются.
3. !important не используется. Исключение: директория vendors.
4. inline-стили не используются.
5. Двойная или более вложенность допускаются в модификаторах и для сложных стилевых эффектов
   (например, ховеры на соседние элементы).

### Правила в компонентах

Именование классов по БЭМу:

1. Вёрстка ведётся от классов (теги, id, атрибуты не используются в качестве селекторов).
2. Каждый компонент является блоком, может содержать в себе как элементы, так и другие сервисные блоки.
3. Для стилизации компонента используется селектор `:host`. Для стилизации модификаторов компонента
   используются сокращенный синтаксис `:host(._modificator)`.
4. Если класс добавляется скриптом, то используется сокращенная запись: `&._selector`.

## ***

### Файловая структура

Все общие стили подключаются в `src/styles.scss`.

```
assets/
  images/
    pic.png
    ...
  fonts/
    OpenMono-Regular.ttf
    ...
  svg/
    leaderboard_24.svg      // Если назвать svg `{$name}_{$size}.svg`, то lucidus 9 сможет их использовать
    qr-code_16.svg
    ...
  styles/                   // Все стили должны быть глобальными и независимыми от контекста
    base/                   // Переносится из продукта в продукт без изменений
      _reset.scss
      _browser.scss
    settings/               // Настройки приложения
      _fonts.scss
      _variables.scss       // Переменные для font, color, transition и т.д 
      _global.scss          // Допустима только вёрстка от тегов
    common/                 // Общие стили (наработки в виде миксинов, универсальных классов и т.д)
      _mixins.scss
      _loader.scss
      ...
    vendors/                // Специфичность перекрывается с помощью !important
      index.scss            // Включает в себя импорты библиотек и переопределения стилей
      _grid.scss
      lucidus/              // По необходимости выносится в отдельную директорию
        _input.scss
        _combobox.scss
        _dropdown.scss
        ...
      ...
```

## ***

### Styleguides

HTML: `/styleguide/styleguide.html`

CSS: `/styleguide/styleguide.scss`
